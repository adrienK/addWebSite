#!/bin/ksh
FLD_VHOST="/etc/httpd_vhost"
FLD_WWW="/var/www"
FLD_CICD="/cicd"
FLD_HTDOC="/htdocs"
FLD_CERTS="/var/dehydrated/certs"

DHDT_DNS="/etc/dehydrated/domains.txt"
DHDT_CONF="/etc/dehydrated/config"
HTTP_CONF="/etc/httpd.conf"

# FUNC
err () {
    msg "[ERR] ${1}"
    exit 1
}

msg()
{
    echo "($(date +%Y-%m-%dT%T%z)) ${1}"
}

# MAIN
msg "Debut du script"

msg "Configuration du domaine et ses alias"
echo -n "--> Nom de domaine : "
read -r domain_name garbage
unset garbage

if [ -d "${FLD_VHOST}/${domain_name}.conf" ]; then
	err "'${FLD_VHOST}/${domain_name}.conf' already exist"
fi

echo -n "--> DNS alias (use space for multiple value) : "
read -r alternate_in

if [[ -n ${alternate_in} ]]; then
    echo -n "--> Redirect alias to master domain [Y/n] "
    read -r alternate_redirect
fi
    
if [[ ${alternate_redirect} == "n" || ${alternate_redirect} == "N" ]] && [[ -n ${alternate_in} ]] ; then
    for als in ${alternate_in}; do
        alias="${alias}      alias \"${als}\"\n"
    done
else
    for als in ${alternate_in}; do
        alias_redirect="${alias_redirect}

        server \"${als}\" {
            listen on * tls port 443
            block return 301 \"https://${domain_name}\"
            tls {
                certificate \"${FLD_CERTS}/${domain_name}/fullchain.pem\"
                key \"${FLD_CERTS}/${domain_name}/privkey.pem\"
            }
        }"
    done
fi

msg "Generation du certificat"
dehydrated --config "${DHDT_CONF}" -c --domain "${domain_name} ${alternate_in}" \
|| err "Impossible de generer le certificat"

msg "Gestion du dossier web"
echo "--> Available repos ⤵"
ls "${FLD_WWW}${FLD_CICD}"
echo -n "--> Selected repos (laisser vide sinon) : "
read -r is_cicd garbage
unset garbage

if [[ -n ${is_cicd} ]]; then
    if [[ ! -d ${FLD_WWW}${FLD_CICD}/${is_cicd} ]] || [[ -z ${is_cicd} ]]; then
        err "Le repos '${is_cicd:=EMPTY}' n'existe pas"
    fi

    echo -n "--> HTTP root file (relative to cicd repos) : "
    read -r in_cicd garbage
    unset garbage

    if [[ ! -d ${FLD_WWW}${FLD_CICD}/${is_cicd}/${in_cicd} ]]; then
        err "Dossier './${is_cicd}/${in_cicd}' n'existe pas"
    fi

    cd "${FLD_WWW}${FLD_HTDOC}" \
    || err "Impossible de ce deplacer dans '${FLD_WWW}${FLD_HTDOC}'"

    ln -s "${FLD_CICD}/${is_cicd}" "${domain_name}" \
    || err "Impossible de creer le symlink pour '${FLD_CICD}/${is_cicd}'"

    [[ -n ${in_cicd} ]] && host_public="/${in_cicd}"
else
    mkdir -p "${FLD_WWW}${FLD_HTDOC}/${domain_name}" \
    || err "Impossible de creer le dossier '.${FLD_HTDOC}/${domain_name}'"

    cat <<EOF >"${FLD_WWW}${FLD_HTDOC}/${domain_name}/index.html" \
    || err "Impossible de creer le fichier '.${FLD_HTDOC}/${domain_name}/index.html'"
<!DOCTYPE html><html style="background-image: 
url(https://m.media-amazon.com/images/M/MV5BMjE1OTc3OTAxN15BMl5BanBnXkFtZTcwMDM2NTUyMw@@._V1_SY1000_CR0,0,1499,1000_AL_.jpg);
height: 100vh;width: 100vw;background-size: cover;overflow: hidden;
background-position: top center;"><head><title>Hello World HTML</title></head>
<body cz-shortcut-listen="true"><h1 style="margin: 35vh auto;
text-align: center;background: rgba(0,0,0,0.3);border-radius: 3px;width: 50vw;
padding: 10vh 0;color: lightcoral;border: 1px solid;">Hello World</h1>
</body></html>
EOF

fi

grep "${domain_name} ${alternate_in}" "${DHDT_DNS}" \
|| echo "${domain_name} ${alternate_in}" >> "${DHDT_DNS}" \
|| err "Impossible d'ecrire dans '${DHDT_DNS}'"

msg "Generation du fichier de conf vhost"

grep "${FLD_VHOST}/${domain_name}.conf" "${HTTP_CONF}" \
|| echo "include \"${FLD_VHOST}/${domain_name}.conf\"" >> "${HTTP_CONF}" \
|| err "Impossible d'ecrire dans '${HTTP_CONF}'"

cat <<EOF >"${FLD_VHOST}/${domain_name}.conf" || err "Impossible de creer '${FLD_VHOST}/${domain_name}.conf'"
server "${domain_name}" {
    $(echo -e "${alias}")

    listen on * tls port 443

    root "${FLD_HTDOC}/${domain_name}${host_public}"
    directory index index.html

    tls {
        certificate "${FLD_CERTS}/${domain_name}/fullchain.pem"
        key "${FLD_CERTS}/${domain_name}/privkey.pem"
    }

    log style combined
    log error "err_${domain_name}.log"
    log access "acc_${domain_name}.log"
}
${alias_redirect}
EOF

msg "Rechargement du deamon HTTPD"
rcctl reload httpd || err "Fail to reload HTTPD"

msg "Fin du script"
exit 0
